import re
from vkbottle import PhotoMessageUploader
from vkbottle import (
    Keyboard,
    KeyboardButtonColor,
    Text,
    Callback,
    template_gen,
    TemplateElement
)
from database import get_groups, get_products_by_groups

back_keyboard = (
    Keyboard(one_time=True, inline=False)
    .add(Text('Меню'), color=KeyboardButtonColor.POSITIVE)
)


def get_menu_keyboard():
    """Клавиатура с группами продуктов"""
    keyboard = Keyboard(inline=True)
    num = 0
    for i, group in enumerate(get_groups()):
        keyboard.add(Callback(f"{group}", {"cmd": f"{group}"}))
        num += 1
        if num == 2 and i != len(get_groups()) - 1:
            keyboard.row()
            num = 0
    return keyboard.get_json()


async def get_carusel(uploader: PhotoMessageUploader, group_title: str) -> None:
    """Карусель из карточек продуктов"""
    elements = []
    for product in get_products_by_groups(group_title):
        attachment = await uploader.upload(product[2])
        id_photo = re.findall(r'-\d+_\d+', attachment)[0]
        keyboard = Keyboard().add(
            Text(f"Купить {product[0]}"), color=KeyboardButtonColor.NEGATIVE
        )
        elements.append(TemplateElement(
            title=product[0],
            description=product[1],
            photo_id=id_photo,
            buttons=keyboard.get_json()
        ))
    return template_gen(*elements)
