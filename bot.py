import os
import random
from envparse import env
from vkbottle.bot import Bot, Message
from vkbottle import (
    GroupEventType,
    GroupTypes,
    BaseStateGroup,
    CtxStorage,
    PhotoMessageUploader
)

from database import init_db, get_groups, get_all_products, BASE_DIR
from services import get_menu_keyboard, back_keyboard, get_carusel

env.read_envfile(os.path.join(BASE_DIR, 'config.env'))
token = env.str('VK_TOKEN')

bot = Bot(token=token)
ctx = CtxStorage()
uploader = PhotoMessageUploader(bot.api)


class ClientState(BaseStateGroup):
    MENU = 0
    PRODUCT = 1


@bot.on.message(state=[ClientState.PRODUCT])
async def product_bought_handler(message: Message):
    await bot.state_dispenser.set(message.peer_id, ClientState.MENU)
    user_info = await bot.api.users.get(message.from_id)
    if any([case in message.text for case in get_all_products()]):
        text = f'{user_info[0].first_name}, поздравляем с покупкой! Наш ' \
               f'менеджер с вами свяжется. А пока можете выбрать еще что-то'
    else:
        text = f'{user_info[0].first_name}, ' \
               f'попробуйте поискать в другой категории'
    await message.answer(
        text,
        keyboard=get_menu_keyboard()
    )


@bot.on.raw_event(GroupEventType.MESSAGE_EVENT, dataclass=GroupTypes.MessageEvent)
async def callback_handler(event: GroupTypes.MessageEvent):
    if event.object.payload["cmd"] in get_groups():
        await bot.state_dispenser.set(event.object.peer_id, ClientState.PRODUCT)
        ctx.set('product', event.object.payload['cmd'])
        user_info = await bot.api.users.get([event.object.peer_id])
        await bot.api.messages.edit(
            conversation_message_id=event.object.conversation_message_id,
            message=ctx.get('product'),
            peer_id=event.object.peer_id,
            template=await get_carusel(uploader, event.object.payload['cmd'])
        )
        await bot.api.messages.send(
            conversation_message_id=event.object.conversation_message_id,
            message=f"{user_info[0].first_name}, выберите товар",
            peer_id=event.object.peer_id,
            keyboard=back_keyboard,
            random_id=random.randint(1, 10*10)
        )


@bot.on.message(state=[ClientState.MENU])
async def choosing_something_handler(message: Message):
    ctx.set('group', message.text)
    user_info = await bot.api.users.get(message.from_id)
    await message.answer(f"{user_info[0].first_name}, берите группу товаров",
                         keyboard=get_menu_keyboard())


@bot.on.message()
async def hello_handler(message: Message):
    user_info = await bot.api.users.get(message.from_id)
    await bot.state_dispenser.set(message.peer_id, ClientState.MENU)
    await message.answer(f'{user_info[0].first_name}, привет! Добро пожаловать '
                         f'в лучшую кондитерскую. Выберите категорию: ',
                         keyboard=get_menu_keyboard())


if __name__ == '__main__':
    init_db()
    bot.run_forever()

