import unittest
import json


from database import (
    init_db,
    get_all_products,
    get_products_by_groups,
    get_groups
)
from services import get_menu_keyboard


class TestDataBase(unittest.TestCase):
    def setUp(self) -> None:
        init_db()

    def test_init_products(self):
        self.assertEqual(10, len(get_all_products()))

    def test_init_groups(self):
        self.assertIn('Напитки', get_groups())

    def test_get_products_by_group(self):
        self.assertIn(
            (
                'Капучино', 'На кокосовом молоке',
                '/home/nikolai/PycharmProjects/vk_bot/media/cappu.jpg'
            ), get_products_by_groups('Напитки')
        )

    def test_keyboard_group(self):
        keyboard_json = get_menu_keyboard()
        self.assertIn(
            {'action':
                 {'label': 'Пирожки', 'payload': {
                     'cmd': 'Пирожки'},
                  'type': 'callback'}
             }, json.loads(keyboard_json)['buttons'][0])

    def test_count_buttons(self):
        keyboard_json = get_menu_keyboard()
        buttons = json.loads(keyboard_json)['buttons']
        count_buttons = len(buttons[0]) + len(buttons[1])
        self.assertEqual(4, count_buttons)
