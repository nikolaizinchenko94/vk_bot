import os
import sqlite3
from typing import List
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

GROUPS = [
    'Пирожки', 'Торты', 'Эклеры', 'Напитки'
]

PRODUCTS = {
    1: [
        ('Пирожок с малиной', 'Вкуснейшая малина', 'pie_raspberry.jpg'),
        ('Пирожок с мясом', 'Мясо курицы', 'pie_meat.jpg'),
        ('Пирожок с творогом', 'Безлактозный творог', 'pie_milk.jpg')
    ],
    2: [
        ('Наполеон', 'Торт на любителя', 'napol.jpeg'),
        ('Птичье молоко', 'Ни одна птица не пострадала', 'cake_bird.jpeg'),
        ('Ягодный', 'Очень много ягод', 'cake_berry.jpeg')
    ],
    3: [
        ('Шоколадный', 'Кремовая начинка', 'eclair_choco.jpg'),
        ('Домашний', 'По рецепту бабушки', 'eclair_home.jpg'),
    ],
    4: [
        ('Капучино', 'На кокосовом молоке', 'cappu.jpg'),
        ('Двойной эспрессо', 'Если надо быстро взбодриться', 'double_es.jpg')
    ]
}


def init_db() -> None:
    """Заполняем базу если она пустая"""
    with sqlite3.connect('bakery.db') as conn:
        cursor = conn.cursor()
        CREATE_TABLE_GROUPS = """
        CREATE TABLE IF NOT EXISTS table_group (
        group_id INTEGER PRIMARY KEY AUTOINCREMENT, 
        title VARCHAR(32) UNIQUE NOT NULL
        )
        """
        cursor.executescript(CREATE_TABLE_GROUPS)
        CREATE_TABLE_PRODUCTS = """
        CREATE TABLE IF NOT EXISTS table_product (
        product_id INTEGER PRIMARY KEY AUTOINCREMENT, 
        title VARCHAR(64) UNIQUE,
        description VARCHAR(256) NOT NULL,
        photo VARCHAR(256) NOT NULL,
        group_id INTEGER NOT NULL,
        FOREIGN KEY (group_id) REFERENCES table_group(group_id)
         ON DELETE CASCADE
        )
        """
        cursor.executescript(CREATE_TABLE_PRODUCTS)
        fill_table_group(cursor)
        fill_table_product(cursor)


def fill_table_group(c: sqlite3.Cursor) -> None:
    c.execute(
        """
        SELECT COUNT(1) FROM table_group
        """)
    count_raws = c.fetchone()[0]
    if not count_raws:
        data = ((group,) for group in GROUPS)
        c.executemany("""
            INSERT INTO table_group (title) VALUES (?)
        """, data
                      )


def fill_table_product(c: sqlite3.Cursor) -> None:
    c.execute(
        """
        SELECT COUNT(1) FROM table_product
        """)
    count_raws = c.fetchone()[0]
    if not count_raws:
        data = [
            (key, product[0], product[1], ''.join([MEDIA_ROOT, product[2]]))
            for (key, value) in PRODUCTS.items()
            for product in value
        ]
        print(data)
        sql_request = """
            INSERT INTO table_product (group_id, title, description, photo) 
            VALUES (?, ?, ?, ?) 
        """
        c.executemany(sql_request, data)


def get_groups() -> List:
    """Достать из БД список названий групп продуктов"""
    with sqlite3.connect('bakery.db') as conn:
        cursor = conn.cursor()
        sql_request = """
        SELECT title FROM table_group
        """
        cursor.execute(sql_request)
        result = cursor.fetchall()
        groups = [group[0] for group in result]
        return groups


def get_products_by_groups(group_title: str) -> List:
    """Достать из БД все продукты переданной группы"""
    with sqlite3.connect('bakery.db') as conn:
        cursor = conn.cursor()
        sql_request = """
        SELECT table_product.title, 
        table_product.description, 
        table_product.photo 
        FROM table_product
        JOIN table_group tg on tg.group_id = table_product.group_id
        WHERE tg.title = ?
        """
        cursor.execute(sql_request, (group_title, ))
        groups = cursor.fetchall()
        return groups


def get_all_products() -> List:
    """Получить список названий всех продуктов"""
    with sqlite3.connect('bakery.db') as conn:
        cursor = conn.cursor()
        sql_request = """
        SELECT title FROM table_product
        """
        result = cursor.execute(sql_request)
        return [i[0] for i in result]
