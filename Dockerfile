FROM python:3.8-alpine

ENV APP_HOME=/usr/src/app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY requirements.txt .

RUN pip install --upgrade pip==21.2.4
RUN pip install -r requirements.txt

COPY . .

ENTRYPOINT ["python3", "/usr/src/app/bot.py"]